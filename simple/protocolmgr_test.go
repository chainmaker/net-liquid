package simple

import (
	"testing"

	"chainmaker.org/chainmaker/net-liquid/core/peer"
	"chainmaker.org/chainmaker/net-liquid/core/protocol"
	"github.com/stretchr/testify/require"
)

func TestSimpleProtocolMgr_RegisterMsgPayloadHandler(t *testing.T) {
	pid := peer.ID("abc")
	pb := newProtocolBook(pid)

	mgr := NewSimpleProtocolMgr(pid, pb).(*simpleProtocolMgr)

	err := mgr.RegisterMsgPayloadHandler("test", func(senderPID peer.ID, msgPayload []byte) {})
	require.NoError(t, err)
	require.True(t, pb.ContainsProtocol(pid, "test"))

	err = mgr.RegisterMsgPayloadHandler("test", func(senderPID peer.ID, msgPayload []byte) {})
	require.Error(t, err)
}

func TestSimpleProtocolMgr_UnregisterMsgPayloadHandler(t *testing.T) {
	pid := peer.ID("abc")
	pb := newProtocolBook(pid)

	mgr := NewSimpleProtocolMgr(pid, pb).(*simpleProtocolMgr)

	err := mgr.UnregisterMsgPayloadHandler("test")
	require.Error(t, err)

	err = mgr.RegisterMsgPayloadHandler("test", func(senderPID peer.ID, msgPayload []byte) {})
	require.NoError(t, err)
	require.True(t, pb.ContainsProtocol(pid, "test"))

	err = mgr.UnregisterMsgPayloadHandler("test")
	require.NoError(t, err)
}

func TestSimpleProtocolMgr_IsRegistered(t *testing.T) {
	pid := peer.ID("abc")
	pb := newProtocolBook(pid)

	mgr := NewSimpleProtocolMgr(pid, pb).(*simpleProtocolMgr)

	ok := mgr.IsRegistered("test")
	require.False(t, ok)

	err := mgr.RegisterMsgPayloadHandler("test", func(senderPID peer.ID, msgPayload []byte) {})
	require.NoError(t, err)
	require.True(t, pb.ContainsProtocol(pid, "test"))

	ok = mgr.IsRegistered("test")
	require.True(t, ok)
}

func TestSimpleProtocolMgr_GetHandler(t *testing.T) {
	pid := peer.ID("abc")
	pb := newProtocolBook(pid)

	mgr := NewSimpleProtocolMgr(pid, pb).(*simpleProtocolMgr)

	h := mgr.GetHandler("test")
	require.Nil(t, h)

	err := mgr.RegisterMsgPayloadHandler("test", func(senderPID peer.ID, msgPayload []byte) {})
	require.NoError(t, err)
	require.True(t, pb.ContainsProtocol(pid, "test"))

	h = mgr.GetHandler("test")
	require.NotNil(t, h)
}

func TestSimpleProtocolMgr_GetSelfSupportedProtocols(t *testing.T) {
	pid := peer.ID("abc")
	pb := newProtocolBook(pid)

	mgr := NewSimpleProtocolMgr(pid, pb).(*simpleProtocolMgr)

	hs := mgr.GetSelfSupportedProtocols()
	require.Equal(t, 0, len(hs))

	err := mgr.RegisterMsgPayloadHandler("test", func(senderPID peer.ID, msgPayload []byte) {})
	require.NoError(t, err)
	require.True(t, pb.ContainsProtocol(pid, "test"))

	hs = mgr.GetSelfSupportedProtocols()
	require.Equal(t, 1, len(hs))
	require.Equal(t, hs[0], protocol.ID("test"))

	err = mgr.RegisterMsgPayloadHandler("test2", func(senderPID peer.ID, msgPayload []byte) {})
	require.NoError(t, err)
	require.True(t, pb.ContainsProtocol(pid, "test2"))

	hs = mgr.GetSelfSupportedProtocols()
	require.Equal(t, 2, len(hs))
	require.Equal(t, hs[0], protocol.ID("test"))
	require.Equal(t, hs[1], protocol.ID("test2"))
}

func TestSimpleProtocolMgr_SetPeerSupportedProtocols(t *testing.T) {
	pid := peer.ID("abc")
	pb := newProtocolBook(pid)

	mgr := NewSimpleProtocolMgr(pid, pb).(*simpleProtocolMgr)

	mgr.SetPeerSupportedProtocols(pid, []protocol.ID{"test", "test2"})
	require.False(t, pb.ContainsProtocol(pid, "test3"))
	require.True(t, pb.ContainsProtocol(pid, "test"))
	require.True(t, pb.ContainsProtocol(pid, "test2"))
}

func TestSimpleProtocolMgr_CleanPeerSupportedProtocols(t *testing.T) {
	pid := peer.ID("abc")
	pb := newProtocolBook(pid)

	mgr := NewSimpleProtocolMgr(pid, pb).(*simpleProtocolMgr)

	mgr.SetPeerSupportedProtocols(pid, []protocol.ID{"test", "test2"})
	require.False(t, pb.ContainsProtocol(pid, "test3"))
	require.True(t, pb.ContainsProtocol(pid, "test"))
	require.True(t, pb.ContainsProtocol(pid, "test2"))

	mgr.CleanPeerSupportedProtocols(pid)
	require.False(t, pb.ContainsProtocol(pid, "test"))
	require.False(t, pb.ContainsProtocol(pid, "test2"))
}

func TestSimpleProtocolMgr_NotifyFuncs(t *testing.T) {
	pid := peer.ID("abc")
	pb := newProtocolBook(pid)

	supportedNotified := make(map[protocol.ID]peer.ID)
	unsupportedNotified := make(map[protocol.ID]peer.ID)

	mgr := NewSimpleProtocolMgr(pid, pb).(*simpleProtocolMgr)

	mgr.SetProtocolSupportedNotifyFunc(func(protocolID protocol.ID, pid peer.ID) {
		supportedNotified[protocolID] = pid
	})

	mgr.SetProtocolUnsupportedNotifyFunc(func(protocolID protocol.ID, pid peer.ID) {
		unsupportedNotified[protocolID] = pid
	})

	mgr.SetPeerSupportedProtocols(pid, []protocol.ID{"test", "test2"})

	_, ok := supportedNotified["test"]
	require.True(t, ok)
	_, ok = supportedNotified["test2"]
	require.True(t, ok)
	_, ok = supportedNotified["test3"]
	require.False(t, ok)
	require.Equal(t, 0, len(unsupportedNotified))

	mgr.SetPeerSupportedProtocols(pid, []protocol.ID{"test3", "test2"})
	_, ok = supportedNotified["test2"]
	require.True(t, ok)
	_, ok = supportedNotified["test3"]
	require.True(t, ok)
	require.Equal(t, 1, len(unsupportedNotified))
	_, ok = unsupportedNotified["test"]
	require.True(t, ok)
}
